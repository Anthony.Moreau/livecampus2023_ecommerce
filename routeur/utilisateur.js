const express = require('express')
const { connectUser, newUser, updateUser, deleteUser, getAllUsers } = require('../model/user')
const routeur = express.Router()
const checkToken = require('../middleware/checkToken')

//GetAll
routeur.get('/', (req, res) => {
    getAllUsers().then(data => {
        res.json(data)
    }).catch(err => {
        res.json(err)
    })
})
//Connect
routeur.post('/connexion', (req, res) => {
    console.log(req.body)
    connectUser(req.body.mail, req.body.pass).then(data => {
        res.json(data)
    }).catch(err => {
        res.json(err)
    })
})
//inscription
routeur.post('/inscription',  (req, res) => {
    newUser(req.body.mail, req.body.pass).then(data => {
        res.json(data)
    }).catch(err => {
        res.json(err)
    })
})
//update
routeur.put('/update/:id',checkToken, (req, res) => {
    if (req.user.niveau === 1) {
        updateUser(req.params.id, req.body.niveau).then(data => {
            res.json(data)
        }).catch(err => {
            res.json(err)
        })
    } else {
        res.status(403).json({ mess: "Vous devez être administrateur" })
    }
})
//Supp
routeur.delete('/delete/:id',checkToken, (req, res) => {
    if (req.user.niveau === 1) {
        deleteUser(req.params.id).then(data => {
            res.json(data)
        }).catch(err => {
            res.json(err)
        })
    } else {
        res.status(403).json({ mess: "Vous devez être administrateur" })
    }
})

module.exports = routeur