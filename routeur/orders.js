const express = require('express')
const checkToken = require('../middleware/checkToken')
const { getAllOrders, getAllOrdersByUser, getOrderLinesById, getOrderById, createOrder, createOrderLine,deleteOrder, deleteOrderLine, updateOrder } = require('../model/order')
const routeur = express.Router()

//get ALL
routeur.get('/', (req, res) => {
    getAllOrders().then(data => {
        res.json(data)
    }).catch(err => {
        res.status(500).json(err)
    })
})

//get ALL by User
routeur.get('/user/:id', (req, res) => {
    getAllOrdersByUser(req.params.id).then(data => {
        res.json(data)
    }).catch(err => {
        res.status(500).json(err)
    })
})

//get order line by ID
routeur.get('/details/:id', (req, res) => {
    getOrderLinesById(req.params.id).then(data => {
        res.json(data)
    }).catch(err => {
        res.status(500).json(err)
    })
})

//get One
routeur.get('/:id', (req, res) => {
    getOrderById(req.params.id).then(data => {
        res.json(data)
    }).catch(err => {
        res.status(500).json(err)
    })
})
//Add
routeur.post('/', checkToken, (req, res) => {
    if (req.user) {
        let currentDateTime = new Date().toLocaleDateString() + " " + new Date().toLocaleTimeString()
        createOrder(currentDateTime, "valide", req.user.mail).then(data => {
            let articleList = req.body.articles
            createOrderLine(data.id, articleList).then(data => {
                res.json(data)
            }).catch(err => {
                res.status(500).json(err)
            });
        }).catch(err => {
            res.status(500).json(err)
        })

    } else {
        res.status(403).json({ mess: "Vous devez être connecté" })
    }
})
//Supprimer
routeur.delete('/:id', checkToken, (req, res) => {
    if (req.user.niveau === 1) {
        deleteOrder(req.params.id).then(data => {
            deleteOrderLine(req.params.id).then(data => {
                res.json(data)
            }).catch(err => {
                res.status(500).json(err)
            });
        }).catch(err => {
            res.status(500).json(err)
        })

    } else {
        res.status(403).json({ mess: "Vous devez être administrateur" })
    }
})
//Update status
routeur.put('/:id', checkToken, (req, res) => {
    if (req.user.niveau === 1) {
        updateOrder(req.params.id, req.body.statut).then(data => {
            res.json(data)
        }).catch(err => {
            res.status(500).json(err)
        })

    } else {
        res.status(403).json({ mess: "Vous devez être administrateur" })
    }
})
module.exports = routeur