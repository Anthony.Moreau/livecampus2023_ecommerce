const db = require("../data/database");
const jwt = require('jsonwebtoken');

function getAllOrders() {
    return new Promise((resolve, rej) => {
        db.all("SELECT * FROM 'order'", (err, res) => {
            if (err) rej(err)
            resolve(res)
        })
    })

}
function getAllOrdersByUser(mailUser) {
    return new Promise((resolve, rej) => {
        db.all("SELECT * FROM 'order' where fk_user=?", mailUser, (err, res) => {
            if (err) rej(err)
            resolve(res)
        })
    })

}
function getOrderById(id) {
    return new Promise((resolve, rej) => {
        db.get("SELECT * FROM 'order' WHERE id=?", id, (err, res) => {
            if (err) rej(err)
            resolve(res)
        })
    })
}
function getOrderLinesById(id) {
    return new Promise((resolve, rej) => {
        db.all("SELECT * FROM 'order' INNER JOIN 'order_line' on 'order_line'.id_command = 'order'.id INNER JOIN 'articles' on 'order_line'.id_article = 'articles'.id WHERE 'order'.id = ?;", id, (err, res) => {
            if (err) rej(err)
            resolve(res)
        })
    })
}
function addOrder(date, statut, fk_user) {
    return new Promise((resolve, rej) => {
        db.run("INSERT INTO 'order' (date,statut,fk_user) VALUES(?,?,?)", [date, statut, fk_use], function (err, res) {
            if (err) rej(err)
            resolve({ mess: "Order ajoutée", id: this.lastID })
        })
    })
}
function createOrder(date, statut, id_user) {
    return new Promise((resolve, rej) => {
        db.run("INSERT INTO 'order' (date,statut,fk_user) VALUES(?,?,?)", [date, statut, id_user], function (err, res) {
            if (err) rej(err)
            resolve({ mess: "Commande créée", id: this.lastID })
        })
    })
}
function createOrderLine(id_command, articleList) {
    return new Promise((resolve, rej) => {
        let rows = ""
        articleList.forEach(article => {
            rows = rows + "("+id_command+","+article.id+","+article.qte+"),"
        });
        rows = rows.slice(0, -1);
        db.run("INSERT INTO 'order_line' (id_command,id_article,qte) VALUES ?".replace('?', rows), function (err, res) {
            if (err) rej(err)
            resolve({ mess: "Ligne de commande créée" })
        })
    })
}
function deleteOrder(id) {
    return new Promise((resolve, rej) => {
        db.run("DELETE FROM 'order' WHERE id=?", id, function (err, res) {
            if (err) rej(err)
            resolve({ mess: "Commande supprimé" })
        })
    })
}
function deleteOrderLine(id_command) {
    return new Promise((resolve, rej) => {
        db.run("DELETE FROM 'order_line' WHERE id_command=?", id_command, function (err, res) {
            if (err) rej(err)
            resolve({ mess: "Ligne(s) de commande supprimée(s)" })
        })
    })
}
function updateOrder(id_command, statut) {
    return new Promise((resolve, rej) => {
        db.run("UPDATE 'order' SET statut=? WHERE id=?", [statut, id_command], (err, res) => {
            if (err) rej(err)
            resolve({ mess: "Commande mise à jour" })
        })
    })
}

module.exports = {
    getAllOrders,
    getOrderById,
    createOrder,
    createOrderLine,
    deleteOrder,
    deleteOrderLine,
    updateOrder,
    getOrderLinesById,
    getAllOrdersByUser
}